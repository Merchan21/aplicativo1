package com.example.miappmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginAdmin extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private EditText Email, Contraseña;
    private Button Login, Registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_admin);

        Email = findViewById(R.id.txtEmail);
        Contraseña = findViewById(R.id.txtPassword);
        Login = findViewById(R.id.btnIngreso);

        mAuth = FirebaseAuth.getInstance();


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = Email.getText().toString();
                String clave = Contraseña.getText().toString();

                if (isValidEmail(correo) && validarContraseña()) {
                    String contraseña = Contraseña.getText().toString();
                    mAuth.signInWithEmailAndPassword(correo, contraseña)
                            .addOnCompleteListener(LoginAdmin.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete( Task<AuthResult> task) {
                                    if (task.isSuccessful()) {

                                        Toast.makeText(LoginAdmin.this, "Inicio de secion como administrador exitoso", Toast.LENGTH_SHORT).show();
                                        nextActivity();


                                    } else {

                                        Toast.makeText(LoginAdmin.this, "ocurrio un error al inicio de sesion", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });
                }
            }
        });
    }

    private boolean isValidEmail (CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean validarContraseña (){
        String contraseña;
        contraseña = Contraseña.getText().toString();
        if (contraseña.length() >=6){
            return true;
        }else return false;

    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            Toast.makeText(this,"Administrador logeado", Toast.LENGTH_SHORT).show();
            nextActivity();
        }else {
        }
    }
    private void nextActivity(){
        startActivity(new Intent(LoginAdmin.this, Homme.class));
        finish();

    }

}